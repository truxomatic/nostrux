all:
	docker build -t nostrux .
	mkdir -p data
	chmod a+rwx data
	echo "to run it :"
	echo '  docker run --rm -it --name nostrux -v $$(pwd):/mnt/nostrux nostrux:latest'
