#!/bin/bash -x

export ROOTDIR="$( cd "$( dirname "$( readlink -e "${BASH_SOURCE[0]}" )" )" && /bin/pwd )"

if test -z "$HOME" ; then
  export HOME=/home/nostrux
fi

if test -s "$HOME/.cargo/env" ; then
  . "$HOME/.cargo/env"
fi

cd ${ROOTDIR}
mkdir -p data/cache/uuids
mkdir -p data/settings
mkdir -p data/logs
cp .screenrc ${HOME}/

cat > /tmp/sample.query <<EOF1
["REQ", "REQUUID", {
  "authors": [],
  "kinds": [1],
  "since": 1671425000,
  "limit": 10
}]
EOF1

if ! test -s data/settings/relay.list ; then
  echo "wss://relay.damus.io" > data/settings/relay.list
fi

if ! test -s data/settings/follow.list ; then
  echo "npub1xtscya34g58tk0z605fvr788k263gsu6cy9x0mhnm87echrgufzsevkk5s" > data/settings/follow.list
fi

rm -f data/cache/follow.list.formatted
while read -r FOLLOW ; do
  if echo "$FOLLOW" | grep -q "^npub" ; then
    export FOLLOW=$(key-convertr --to-hex "$FOLLOW")
  fi
  echo "$FOLLOW" >> data/cache/follow.list.formatted
done < data/settings/follow.list

cat data/cache/follow.list.formatted | sort | uniq > data/cache/follow.list.formatted.uniq
while read -r FOLLOW ; do
  export FOLLOW=$(echo "$FOLLOW" | sed "s=#.*==g")
  if echo "$FOLLOW" | grep -q ........ ; then
    if echo "$FOLLOW" | grep -q "^npub" ; then
      export FOLLOW=$(key-convertr --to-hex "$FOLLOW")
    fi
    cat /tmp/sample.query | jq --arg NEWID "${FOLLOW}" 'map((select(..|.authors?) | .authors) |= . + [$NEWID])' > /tmp/sample.query.new
    mv -f /tmp/sample.query.new /tmp/sample.query
  fi
done < data/cache/follow.list.formatted.uniq

while read -r RELAY ; do
  export RELAY_URI=$(echo "$RELAY" | tr -d -c "[:alnum:]\-_./:")
  export RELAY_STRING=$(echo "$RELAY" | tr -d -c "[:alnum:]\-_.")
  screen -S nostrux_relay_${RELAY_STRING} -e^Oo -m -d -ln -h 8192 ./nostrux_relay.sh "${RELAY_URI}" "${RELAY_STRING}"
done < data/settings/relay.list

while true ; do
  screen -ls
  ls -lrt data/logs
  date
  sleep 31
done
