FROM debian:11

## to build :
## docker build -t nostrux .

## to run :
## docker run --rm -it --name nostrux -v $(pwd):/mnt/nostrux nostrux:latest'


RUN useradd --create-home --uid 1000 nostrux

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /home/nostrux

RUN mkdir -p /mnt
RUN apt update
RUN apt install -y apcalc apt-transport-https apt-utils aptitude autoconf automake build-essential ca-certificates ccrypt curl emacs-nox git iotop iptables iputils-ping jq less net-tools netcat-openbsd procps psmisc pv rsync screen socat software-properties-common strace tcpdump tcsh telnet unzip uuid vim wget xxd zip
RUN apt install -y pkg-config libssl-dev file moreutils

USER nostrux:1000
WORKDIR "${HOME}"
RUN mkdir -p bin

RUN curl https://sh.rustup.rs -sSf > "${HOME}/rustup.rs.sh"
RUN md5sum "${HOME}/rustup.rs.sh" | grep -w 11e8a832eefd5d39f1b4b9a661961d4b
RUN sh "${HOME}/rustup.rs.sh" -y
RUN echo '. "$HOME/.cargo/env"' >> "$HOME/.profile"

RUN git clone https://github.com/vi/websocat.git
RUN . "$HOME/.cargo/env" && cd websocat && git checkout 391f7f6fb83a145eaecdb4f09c8f443735a586f3 && cargo build --release --features=ssl
RUN cp -vf websocat/target/release/websocat "$HOME/bin"

RUN git clone https://github.com/rot13maxi/key-convertr.git
RUN . "$HOME/.cargo/env" && cd key-convertr && git checkout 12d2bcf43f68773bb46cec411a0ea2845dbe8bae && cargo install --path .

CMD /mnt/nostrux/nostrux.sh
