#!/bin/bash -x

export RELAY_URI=$1
export RELAY_STRING=$2

exec > >(ts "%.s" | tee data/logs/relay.${RELAY_STRING}.log) 2>&1

## comment or uncomment the "rm".  If the uuid persists, then we may not re-receive data that we got on previous runs
rm -f data/cache/uuids/subscription_id.${RELAY_STRING}
if test -s data/cache/uuids/subscription_id.${RELAY_STRING} ; then
   export REQUUID=$(cat data/cache/uuids/subscription_id.${RELAY_STRING})
else
   export REQUUID=$(uuid)
   echo "$REQUUID" > data/cache/uuids/subscription_id.${RELAY_STRING}
fi

cat /tmp/sample.query | sed "s=REQUUID=${REQUUID}=g" > data/cache/query.${RELAY_STRING}

cat data/cache/query.${RELAY_STRING}
tail --bytes=+0 -F data/cache/query.${RELAY_STRING} | stdbuf --input=0 --output=0 ./trim-json.jq | ${HOME}/bin/websocat -v -E --text --ping-interval 32 --ping-timeout 100 "${RELAY_URI}"

